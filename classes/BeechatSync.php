<?php

class BeechatSync {
        static function onFriendCreate($event, $object_type, $relationship) {
		if ($relationship->relationship == 'friendrequest') {
			elgg_load_library('elgg:beechat');
                        $friend = get_entity($relationship->guid_two);
                        // create friend request
			ejabberd_friend_request(elgg_get_logged_in_user_entity(), $friend);
		}
	}
        static function onFriendDelete($event, $object_type, $relationship) {
		if ($relationship->relationship == 'friendrequest') {
			elgg_load_library('elgg:beechat');
                        $subject = get_entity($relationship->guid_two);
			// here friend is guid_one because is the one initiating
                        $friend = get_entity($relationship->guid_one);
                        $friends = $friend->isFriendsWith($subject->guid);
                        if ($friends) {
                                // accept friend request
                		ejabberd_friend_accept(elgg_get_logged_in_user_entity(), $friend);
                        } else {
                                // decline friend request
				ejabberd_friend_deny(elgg_get_logged_in_user_entity(), $friend);
                        }
                }
                elseif ($relationship->relationship == 'friend') {
			elgg_load_library('elgg:beechat');
                        $subject = get_entity($relationship->guid_one);
                        $friend = get_entity($relationship->guid_two);
                        // delete friendship
			ejabberd_friend_remove(elgg_get_logged_in_user_entity(), $friend);
                }

	}

}
