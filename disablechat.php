<?php
        require_once(dirname(dirname(dirname(__FILE__))) . "/engine/start.php");
	if (elgg_is_logged_in()) {
		elgg_get_logged_in_user_entity()->chatenabled = false;
		system_message(elgg_echo("beechat:disabled"));
	}
	forward($_SERVER['HTTP_REFERER']);
?>
