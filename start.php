<?php
	/**
	 * Beechat
	 * 
	 * @package beechat
	 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0.html GNU Public License version 2
	 * @author Beechannels <contact@beechannels.com>
	 * @copyright Beechannels 2007-2010
	 * @link http://beechannels.com/
	 */

	GLOBAL $CONFIG;
	
	function beechat_create_group($event, $object_type, $object)
	{
		elgg_load_library('elgg:beechat');
		ejabberd_create_group($object);
	}

	function beechat_delete_group($event, $object_type, $object)
	{
		elgg_load_library('elgg:beechat');
		ejabberd_destroy_group($object);
	}

	function beechat_member_add($event, $object_type, $object)
        {
		if ($object->relationship === "member") {
			elgg_load_library('elgg:beechat');
			$user = get_entity($object->guid_one);
			$group = get_entity($object->guid_two);
			$room = new EjabberdMucRoom($group);
			$room->addMember($user);
		}
        }

	function beechat_member_delete($event, $object_type, $object)
        {
		if ($object->relationship === "member") {
			elgg_load_library('elgg:beechat');
			$user = get_entity($object->guid_one);
			$group = get_entity($object->guid_two);
			$room = new EjabberdMucRoom($group);
			$room->setAffiliation($user, "none");
		}
        }

	function beechat_init()
	{
		$pluginspath = elgg_get_plugins_path();
		elgg_register_library('elgg:beechat', $pluginspath . 'beechat/lib/beechat.php');

		elgg_register_event_handler('pagesetup', 'system', 'beechat_pagesetup');
		// group actions disabled for now
		/*if (elgg_get_plugin_setting("groupdomain", "beechat")) {
			register_elgg_event_handler('create', 'group', 'beechat_create_group');
			register_elgg_event_handler('delete', 'group', 'beechat_delete_group');
		}
		register_elgg_event_handler('create', 'member', 'beechat_member_add');
		register_elgg_event_handler('delete', 'relationship', 'beechat_member_delete');*/


		$actions_path = $pluginspath . 'beechat/actions/';
		//elgg_register_action('beechat/join_groupchat', $actions_path . 'join_groupchat.php');
		//elgg_register_action('beechat/leave_groupchat', $actions_path . 'leave_groupchat.php');
		elgg_register_action('beechat/get_statuses', $actions_path . 'get_statuses.php');
		elgg_register_action('beechat/get_icons', $actions_path . 'get_icons.php');
		elgg_register_action('beechat/get_details', $actions_path . 'get_details.php');
		elgg_register_action('beechat/get_connection', $actions_path . 'get_connection.php');
		elgg_register_action('beechat/get_state', $actions_path . 'get_state.php');
		elgg_register_action('beechat/save_state', $actions_path . 'save_state.php');

		/*register_elgg_event_handler('create', 'friendrequest', 'beechat_xmpp_add_friendx');
		#register_plugin_hook('action', 'friends/add', 'beechat_xmpp_add_friend', 1000);
		register_plugin_hook('river_update', 'river_update', 'beechat_xmpp_approve_friendx');
		register_plugin_hook('river_update_foreign', 'river_update', 'beechat_xmpp_approve_friendx');
		#register_plugin_hook('action', 'friendrequest/approve', 'beechat_xmpp_approve_friend', 1000);


		register_plugin_hook('action', 'friendrequest/decline', 'beechat_xmpp_decline_friend', 1000);
		register_plugin_hook('action', 'friends/remove', 'beechat_xmpp_remove_friend', 1000);*/

		// new friend sync
		elgg_register_event_handler('delete', 'friend', array('BeechatSync', 'onFriendDelete'));
	        //elgg_register_event_handler('create', 'friendrequest', array('BeechatSync', 'onFriendCreate'));
        	elgg_register_event_handler('delete', 'friendrequest', array('BeechatSync', 'onFriendDelete'));


	
		elgg_extend_view('js/initialise_elgg', 'js/json2.js');
		elgg_extend_view('js/initialise_elgg', 'js/jquery.cookie.min.js');
		elgg_extend_view('js/initialise_elgg', 'js/jquery.scrollTo-min.js');
		elgg_extend_view('js/initialise_elgg', 'js/jquery.serialScroll-min.js');
		elgg_extend_view('js/initialise_elgg', 'js/b64.js');
		elgg_extend_view('js/initialise_elgg', 'js/sha1.js');
		elgg_extend_view('js/initialise_elgg', 'js/md5.js');
		elgg_extend_view('js/initialise_elgg', 'js/strophe.min.js');
		elgg_extend_view('js/initialise_elgg', 'js/strophe.muc.js');
		elgg_extend_view('js/initialise_elgg', 'js/jquery.tools.min.js');
		elgg_extend_view('css', 'beechat/screen.css');
		elgg_extend_view('js/initialise_elgg', 'beechat/beechat.js');
		elgg_extend_view('page/elements/head', 'beechat/beechat.userjs');
		
		elgg_extend_view('page/elements/foot', 'beechat/beechat');

		$domain = elgg_get_plugin_setting("domain", "beechat");
		//$group_domain = elgg_get_plugin_setting("groupdomain", "beechat");
		/*$dbname = elgg_get_plugin_setting("dbname", "beechat");
		$dbhost = elgg_get_plugin_setting("dbhost", "beechat");
		$dbuser = elgg_get_plugin_setting("dbuser", "beechat");
		$dbpassword = elgg_get_plugin_setting("dbpassword", "beechat");*/

		global $CONFIG;
		$CONFIG->chatsettings['domain'] = $domain;
		//$CONFIG->chatsettings['groupdomain'] = $group_domain;

		register_notification_handler('xmpp', 'beechat_notifications');
	//	register_plugin_hook('notify:entity:message','object','beechat_notifications_msg');
	}

	function beechat_notifications($from, $to, $subject, $topic, $params = array()) {
		ejabberd_send_chat($to, "<div>".$topic."</div>");
	}


	function beechat_friendly_title($title) {
		// need this because otherwise seems elgg
		// gets in some problem trying to call the view
		//$title = iconv('UTF-8', 'ASCII//TRANSLIT', $title);
		$title = preg_replace("/[^\w ]/","",$title);
		$title = str_replace(" ","-",$title);
		$title = str_replace("--","-",$title);
		$title = trim($title);
		$title = strtolower($title);
		return $title;
	}

	function beechat_pagesetup()
	{
		global $CONFIG;
		/*if (elgg_get_context() == 'group_profile' && elgg_is_logged_in()) {
			if (elgg_get_plugin_setting("groupdomain", "beechat")) {
				$user = elgg_get_logged_in_user_entity();
				$group = elgg_get_page_owner_entity();
				if (!$group || !($group instanceof ElggGroup))
					return;
				if ($user->chatenabled && elgg_get_plugin_setting("groupdomain", "beechat")) {
					if ($group->isPublicMembership() || $group->isMember($user)) {
						$item = new ElggMenuItem('chatroom', elgg_echo('beechat:chatroom'), "javascript:g_beechat_user.joinRoom('".beechat_friendly_title($group->name)."@".$CONFIG->chatsettings['groupdomain']."', '".$group->guid."')");
						elgg_register_menu_item('page', 'item');
					}
				}
			}
		}
		else*/if (elgg_get_context() == 'settings' && elgg_is_logged_in()) {
			$is_enabled = elgg_get_logged_in_user_entity()->chatenabled;
			$action = $is_enabled ? 'disable' : 'enable';
			elgg_register_menu_item('page', array(
				'name' => 'beechat',
				'text'=> elgg_echo("beechat:{$action}chat"),
				'href' => $CONFIG->wwwroot . "mod/beechat/{$action}chat.php",
			));
		}
	}


	function ejabberd_send_chat($user, $body) { // $user adds $friend
		$from = 'notify@'.elgg_get_plugin_setting("domain", "beechat").'/net';
		if ($user->alias) {
			
		}
		elgg_load_library('elgg:beechat');
		$to = ejabberd_getjid($user, true);
                //xmlrpc_set_type(&$body, "base64"); 
		$param = array("body" => $body,
				"from" => $from,
				"to" => $to);
		ejabberd_xmlrpc_command('send_html_message', $param);
	}




elgg_register_event_handler('init', 'system', 'beechat_init');
